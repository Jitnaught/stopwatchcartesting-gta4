This is a mod very useful for people testing their cars for their acceleration, and getting the speed their car drives most of the time (average speed).

How to install:
First install .NET Scripthook, then copy StopwatchCarTesting.net.dll and StopwatchCarTesting.ini to the scripts folder in your GTA IV directory.

Controls:
Right-CTRL + S = Toggle the menu
Enter = Start/stop the test
NumPad-Plus = Increase speed to stop test at
NumPad-Minus = Decrease speed to stop test at
-Keys are changeable in the ini file

How to use:
1. Open the menu with Right-CTRL + S
2. Set the speed to stop the test at with NumPad-Plus and NumPad-Minus
3. Start the test by pressing Enter
4. Drive till you hit that speed
5. If you can't hit it, then press Enter to stop the test, then change the speed to stop at again.

Credits:
Damage Inc.: for requesting the script
LetsPlayOrDy (now named Jitnaught): for creating the script

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
