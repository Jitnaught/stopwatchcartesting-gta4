﻿using GTA;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace StopwatchCarTesting
{
    public class StopwatchCarTesting : Script
    {
        bool menuOpen = false, useHoldKey;
        Keys menuTogglePressKey, menuToggleHoldKey, stopwatchStartStopKey, increaseSpeedToStopAt, decreaseSpeedToStopAt;
        Stopwatch stopwatch = new Stopwatch();
        List<float> speeds = new List<float>();
        float speedToStopAt = 60.0f;

        public StopwatchCarTesting()//mph or kph; start stopwatch when start driving (speed over 2 or something); limit the speed to stop from 10 to 170 for mph, 10 to * for kph; hold key down for speed to stop key; font customizable
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue("menu_toggle_press_key", "keys", Keys.S);
                Settings.SetValue("menu_toggle_hold_key", "keys", Keys.RControlKey);
                Settings.SetValue("use_menu_toggle_hold_key", "keys", true);
                Settings.SetValue("stopwatch_start_stop_key", "keys", Keys.Enter);
                Settings.SetValue("increase_speed_to_stop_at", "keys", Keys.Add);
                Settings.SetValue("decrease_speed_to_stop_at", "keys", Keys.Subtract);

                Settings.Save();
            }

            menuTogglePressKey = Settings.GetValueKey("menu_toggle_press_key", "keys", Keys.S);
            menuToggleHoldKey = Settings.GetValueKey("menu_toggle_hold_key", "keys", Keys.RControlKey);
            useHoldKey = Settings.GetValueBool("use_menu_toggle_hold_key", "keys", true);
            stopwatchStartStopKey = Settings.GetValueKey("stopwatch_start_stop_key", "keys", Keys.Enter);
            increaseSpeedToStopAt = Settings.GetValueKey("increase_speed_to_stop_at", "keys", Keys.Add);
            decreaseSpeedToStopAt = Settings.GetValueKey("decrease_speed_to_stop_at", "keys", Keys.Subtract);

            KeyDown += StopwatchCarTesting_KeyDown;
            PerFrameDrawing += StopwatchCarTesting_PerFrameDrawing;
            new GTA.Timer(500, true).Tick += SpeedRecorder_Tick;
            new GTA.Timer(100, true).Tick += SpeedChecker_Tick;
        }

        private void SpeedChecker_Tick(object sender, EventArgs e)
        {
            if (menuOpen && Player.Character.isInVehicle() && stopwatch.IsRunning)
            {
                if (Player.Character.CurrentVehicle.Speed * 2.2f >= speedToStopAt) stopwatch.Stop();
            }
        }

        private void SpeedRecorder_Tick(object sender, EventArgs e)
        {
            if (menuOpen && Player.Character.isInVehicle() && stopwatch.IsRunning)
            {
                speeds.Add(Player.Character.CurrentVehicle.Speed * 2.2f);
            }
        }

        
        void StopwatchCarTesting_PerFrameDrawing(object sender, GraphicsEventArgs e)
        {
            if (menuOpen && Player.Character.isInVehicle())
            {
                e.Graphics.DrawText("Speed to stop at: " + speedToStopAt.ToString(), 20, 20, Color.White);
                string endUserString = string.Format("{0}:{1:00}:{2:00}:{3:00}.{4:000}", stopwatch.Elapsed.Days, stopwatch.Elapsed.Hours, stopwatch.Elapsed.Minutes, stopwatch.Elapsed.Seconds, stopwatch.Elapsed.Milliseconds);
                e.Graphics.DrawText(endUserString, 20, 50, Color.White);
                if (!stopwatch.IsRunning)
                {
                    if (stopwatch.ElapsedMilliseconds > 0)
                    {
                        float averageSpeed = 0;
                        foreach (float speed in speeds)
                        {
                            averageSpeed += speed;
                        }
                        averageSpeed = averageSpeed / speeds.Count;
                        e.Graphics.DrawText("Average speed: " + averageSpeed.ToString(), 20, 80, Color.White);
                    }
                }
            }
            else if (stopwatch.IsRunning) stopwatch.Reset();
        }

        void StopwatchCarTesting_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if (Player.Character.isInVehicle())
            {
                if ((!useHoldKey || isKeyPressed(menuToggleHoldKey)) && e.Key == menuTogglePressKey)
                {
                    menuOpen = !menuOpen;
                    if (!menuOpen && stopwatch.ElapsedMilliseconds > 0) stopwatch.Reset();
                }

                if (menuOpen)
                {
                    if (e.Key == stopwatchStartStopKey)
                    {
                        if (stopwatch.IsRunning)
                        {
                            stopwatch.Stop();
                        }
                        else
                        {
                            if (stopwatch.ElapsedMilliseconds > 0)
                            {
                                speeds.Clear();
                                stopwatch.Reset();
                            }
                            stopwatch.Start();
                        }
                    }
                    else if (e.Key == increaseSpeedToStopAt && !stopwatch.IsRunning)
                    {
                        speedToStopAt++;
                    }
                    else if (e.Key == decreaseSpeedToStopAt && !stopwatch.IsRunning)
                    {
                        speedToStopAt--;
                    }
                }
            }
        }

        private void Subtitle(string info, int millisecs = 1500)
        {
            GTA.Native.Function.Call("PRINT_STRING_WITH_LITERAL_STRING_NOW", "STRING", info, millisecs, 1);
        }
    }
}
